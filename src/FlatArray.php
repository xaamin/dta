<?php
namespace Xaamin\Dta;

class FlatArray
{
    protected $data;
    protected $complex = false;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function make()
    {
        return $this->buildSingle($this->data);
    }

    public function all($value = true)
    {
        $this->complex = boolval($value);
    }

    protected function buildSingle(array $values)
    {
        $result = [];

        foreach ($values as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->buildComplex($value));
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    protected function buildComplex(array $values)
    {
        $result = [];

        foreach ($values as $key => $value) {
            // Regla: Todos los índices no numéricos deben ir primero
            if (is_numeric($key)) {
                if (!$this->complex && $key === 0) {
                    $result = array_merge($result, $this->buildSingle($value));

                    break;
                } else if ($this->complex) {
                    $result[$key] = array_merge($result, $this->buildSingle($value));
                }
            } else if (is_array($value)) {
                if ($this->complex) {
                    if (!array_is_associative($value)) {
                        $result[$key] = array_merge($result, $this->buildComplex($value));
                    } else {
                        $result[$key] = array_merge($result, $this->buildSingle($value));
                    }
                } else {
                    $value = !array_is_associative($value) ? $value[0] : $value;

                    $result = array_merge($result, $this->buildSingle($value));
                }
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}