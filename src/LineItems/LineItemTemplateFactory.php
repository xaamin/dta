<?php
namespace Xaamin\Dta\LineItems;

class LineItemTemplateFactory
{
    public function make(array $values, $separator)
    {
        $template = [];

        foreach ($values as $value) {
            $segments = array_map('trim', explode($separator, $value));

            $name = array_shift($segments);

            $template[$name] = $segments;
        }

        return $template;
    }
}