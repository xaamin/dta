<?php
namespace Xaamin\Dta\LineItems;

class LineItemGroupParser
{
    use WithLineItemParser;

    public function make($key, array $values)
    {
        $result = [];

        list($groups, $values) = $this->getGroups($key, $this->bones, $values);

        $result[$key] = [];

        // Calculate groups
        foreach ($groups as $group) {
            if (in_array($key, $this->unbounded, true)) {
                list($line) = (new LineItemParser)
                    ->withSeparator($this->separator)
                    ->withTemplate($this->template)
                    ->withBones($this->bones)
                    ->withUnbounded($this->unbounded)
                    ->withRoot($key)
                    ->make($group);

                $result[$key][] = $line;
            } else {
                list($line) = (new LineItemParser)
                    ->withSeparator($this->separator)
                    ->withTemplate($this->template)
                    ->withBones($this->bones)
                    ->withUnbounded($this->unbounded)
                    ->make($group);

                $result[$key] = $line;
            }
        }

        if (empty($result[$key])) {
            unset($result[$key]);
        }

        return [$result, $values];
    }

    protected function getGroups($name, $template, array $values)
    {
        $groups = [];
        $tmpGroup = [];

        $template = $this->getNestedTemplate($template);

        foreach ($values as $index => $value) {
            // Root node
            if (string_starts_with($value, "{$name}{$this->separator}")) {
                if (!empty($tmpGroup)) {
                    $groups[] = $tmpGroup;
                    $tmpGroup = [];
                }

                $tmpGroup[] = $value;

                unset($values[$index]);
            } else {
                // Nested nodes
                foreach ($template as $property) {
                    if (is_string($property) && string_starts_with($value, "{$property}{$this->separator}") && !string_starts_with($value, "{$name}{$this->separator}")) {
                        $tmpGroup[] = $value;

                        unset($values[$index]);
                    }
                }
            }
        }

        if (!empty($tmpGroup)) {
            $groups[] = $tmpGroup;

            unset($tmpGroup);
        }

        return [$groups, $values];
    }

    protected function getNestedTemplate(array $template)
    {
        $newTemplate = [];

        foreach ($template as $key => $value) {
            if (is_string($value)) {
                $newTemplate[] = $value;
            } else if (is_array($value)) {
                $newTemplate = array_merge($newTemplate, $this->getNestedTemplate($value));
            }
        }

        return $newTemplate;
    }

}