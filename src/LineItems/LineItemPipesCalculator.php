<?php
namespace Xaamin\Dta\LineItems;

class LineItemPipesCalculator
{
    protected $pipes = [];
    protected $content;

    public function pipes($pipes)
    {
        $this->pipes = $pipes;

        return $this;
    }

    public function with(array $content)
    {
        $this->content = $content;

        return $this;
    }

    public function make(array $pipes)
    {
        $result = [];

        foreach ($pipes as $pipe) {
            if ($this->isAllowed($pipe)) {
                $result[] = $pipe;
            }
        }

        return $result;
    }

    protected function isAllowed($pipe)
    {
        foreach ($this->content as $line) {
            if (string_starts_with($line, $pipe)) {
                return true;
            }
        }

        return false;
    }
}