<?php
namespace Xaamin\Dta\LineItems;

class LineItemParser
{
    use WithLineItemParser;

    public function make(array $values)
    {
        $result = [];

        foreach ($this->bones as $key => $value) {
            if (is_string($value)) {
                $bones = $this->template[$value] ?? [];

                while (true) {
                    list($row, $values) = $this->getLineItem($value, $values);

                    if ($row === null) {
                        break;
                    }

                    $result = $this->makeResult($value, $result, $bones, $row);
                }

            } else {
                list($groups, $values) = (new LineItemGroupParser)
                    ->withSeparator($this->separator)
                    ->withTemplate($this->template)
                    ->withBones($value)
                    ->withUnbounded($this->unbounded)
                    ->make($key, $values);

                $result = array_merge($result, $groups);
            }
        }

        return [$result, $values];
    }

    protected function getLineItem($key, array $values)
    {
        $rest = $values;
        $row = null;

        foreach ($values as $index => $value) {
            if (string_starts_with($value, "{$key}{$this->separator}")) {
                $segments = array_map('trim', explode($this->separator, $value));

                // Removes the property name
                array_shift($segments);
                // Rest are the node properties
                $row = $segments;

                unset($rest[$index]);

                break;
            }
        }

        unset($values);

        return [$row, $rest];
    }

    protected function makeResult($key, array $result, $bones, array $row)
    {
        if (!isset($result[$key])) {
            $result[$key] = [];
        }

        $node = [];

        foreach ($bones as $property) {
            $node[$property] = array_shift($row);
        }

        if ($this->isPrimitive($key)) {
            $value = !is_array($node) ? $node : array_values($node)[0] ?? null;

            if (is_array($result[$key]) && empty($result[$key])) {
                $result[$key] = $this->isUnbounded($key) ? [$value] : $value;
            } else {
                if (!is_array($result[$key])) {
                    $result[$key] = [$result[$key]];
                }

                $result[$key][] = $value;
            }
        } elseif ($this->isUnbounded($key) && !$this->isRootNode($key)) {
            $result[$key][] = $node;
        } else {
            if (empty($result[$key])) {
                $result[$key] = $node;
            } else {
                if (!isset($result[$key][0])) {
                    $result[$key] = [$node];
                }

                $result[$key][] = $node;
            }
        }

        if (empty($result[$key])) {
            unset($result[$key]);
        }

        return $result;
    }
}
