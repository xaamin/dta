<?php
namespace Xaamin\Dta\LineItems;

trait WithLineItemParser
{
    protected $root;
    protected $template;
    protected $bones = [];
    protected $unbounded = [];
    protected $primitives = [];
    protected $separator = '';

    public function withSeparator(string $separator)
    {
        $this->separator = $separator;

        return $this;
    }

    public function withBones(array $bones)
    {
        $this->bones = $bones;

        return $this;
    }

    public function withUnbounded(array $unbounded)
    {
        $this->unbounded = $unbounded;

        return $this;
    }

    public function withPrimitives(array $primitives)
    {
        $this->primitives = $primitives;

        return $this;
    }

    public function withRoot($name)
    {
        $this->root = $name;

        return $this;
    }

    protected function isRootNode($name)
    {
        return $this->root === $name;
    }

    public function withTemplate(array $template)
    {
        $this->template = $template;

        $this->bones = array_merge($this->bones, array_keys($template));

        return $this;
    }

    protected function isUnbounded($name)
    {
        return in_array($name, $this->unbounded, true);
    }

    protected function isPrimitive($name)
    {
        return in_array($name, $this->primitives, true);
    }
}
