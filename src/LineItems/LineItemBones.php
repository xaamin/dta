<?php
namespace Xaamin\Dta\LineItems;

use Xaamin\Dta\Contracts\LineItemBonesInterface;

abstract class LineItemBones implements LineItemBonesInterface
{
    protected $content;

    abstract public function getName();

    abstract public function getBones();

    abstract public function getUnbounded();

    public function getPrimitives()
    {
        return [];
    }

    public function make(array $bones, array $unbounded, array $primitives = [])
    {
        return [
            array_merge($bones, $this->getBones()),
            array_merge($unbounded, $this->getUnbounded()),
            array_merge($primitives, $this->getPrimitives())
        ];
    }
}
