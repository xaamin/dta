<?php
namespace Xaamin\Dta\LineItems;

use UnexpectedValueException;
use Xaamin\Dta\Contracts\LineItemBonesInterface;

class LineItemBonesManager
{
    protected $strategies = [];

    public function extend(LineItemBonesInterface $strategy)
    {
        $this->strategies[$strategy->getName()] = $strategy;

        return $this;
    }

    public function make(array $pipes)
    {
        $bones = [];
        $unbounded = [];
        $primitives = [];

        foreach ($pipes as $pipe) {
            $strategy = $this->strategies[$pipe] ?? null;

            if (!$strategy) {
                throw new UnexpectedValueException("The {$pipe} bones generator is not registered.");
            }

            list($bones, $unbounded, $primitives) = $strategy->make($bones, $unbounded, $primitives);
        }

        return [$bones, $unbounded, $primitives];
    }
}
