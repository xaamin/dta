<?php
namespace Xaamin\Dta\Contracts;

interface TemplateChainStepInterface
{
    public function getName();

    public function withData(array $data);

    public function execute(array $values);
}
