<?php
namespace Xaamin\Dta\Contracts;

interface LineItemBonesInterface
{
    public function getName();

    public function getBones();

    public function make(array $bones, array $unbounded);
}