<?php
namespace Xaamin\Dta\Casting;

abstract class Casting
{
    abstract function getName();

    abstract public function make($value, array $params = []);
}