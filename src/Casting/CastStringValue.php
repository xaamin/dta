<?php
namespace Xaamin\Dta\Casting;

class CastStringValue extends Casting
{
    public function getName()
    {
        return 'string';
    }

    public function make($value, array $params = [])
    {
        if (is_string($value)) {
            return $value;
        }

        return !is_string($value) ? strval($value) : null;
    }
}