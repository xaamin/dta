<?php
namespace Xaamin\Dta\Casting;

use UnexpectedValueException;

class CastingManager
{
    protected $strategies = [];

    public function extend(Casting $strategy)
    {
        $this->strategies[$strategy->getName()] = $strategy;
    }

    public function getStrategies()
    {
        return $this->strategies;
    }

    public function get($name)
    {
        $strategy = $this->strategies[$name] ?? null;

        if (!$strategy) {
            throw new UnexpectedValueException("Strategy {$name} is not defined for casting values");
        }

        return $strategy;
    }
}