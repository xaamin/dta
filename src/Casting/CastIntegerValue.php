<?php
namespace Xaamin\Dta\Casting;

class CastIntegerValue extends Casting
{
    public function getName()
    {
        return 'integer';
    }

    public function make($value, array $params = [])
    {
        return is_numeric($value) ? intval($value) : null;
    }
}