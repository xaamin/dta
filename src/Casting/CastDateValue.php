<?php
namespace Xaamin\Dta\Casting;

use DateTimeInterface;

class CastDateValue extends Casting
{
    public function getName()
    {
        return 'date';
    }

    public function make($value, array $params = [])
    {
        $format = $params[0] ?? 'Y-m-d';

        return $value instanceof DateTimeInterface ? $value->format($format) : $value;
    }
}
