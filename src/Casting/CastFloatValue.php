<?php
namespace Xaamin\Dta\Casting;

class CastFloatValue extends Casting
{
    public function getName()
    {
        return 'float';
    }

    public function make($value, array $params = [])
    {
        if (is_string($value)) {
            $value = str_replace(',', '', $value);
            $value = preg_replace('/\s+/', '', $value);

            $value = trim($value);
        }

        return is_numeric($value) ? floatval($value) : null;
    }
}