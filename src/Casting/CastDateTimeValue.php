<?php
namespace Xaamin\Dta\Casting;

use DateTimeInterface;

class CastDateTimeValue extends Casting
{
    public function getName()
    {
        return 'datetime';
    }

    public function make($value, array $params = [])
    {
        $format = $params[0] ?? 'Y-m-d H:i:s';

        return $value instanceof DateTimeInterface ? $value->format($format) : $value;
    }
}
