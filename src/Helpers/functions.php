<?php

if (!function_exists('array_is_associative')) {
     /**
     * Determines if an array is associative.
     *
     * An array is "associative" if it doesn't have sequential numerical keys beginning with zero.
     *
     * @param  array  $array
     * @return bool
     */
    function array_is_associative(array $array)
    {
        $keys = array_keys($array);

        return array_keys($keys) !== $keys;
    }
}

if (!function_exists('string_starts_with')) {
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string  $haystack
     * @param  string  $needle
     * @return bool
     */
    function string_starts_with($haystack, $needle)
    {
        if ((string) $needle !== '' && strncmp($haystack, $needle, strlen($needle)) === 0) {
            return true;
        }

        return false;
    }
}