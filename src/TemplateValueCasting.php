<?php
namespace Xaamin\Dta;

use LogicException;
use Xaamin\Dta\Casting\CastDateValue;
use Xaamin\Dta\Casting\CastingManager;
use Xaamin\Dta\Casting\CastFloatValue;
use Xaamin\Dta\Casting\CastStringValue;
use Xaamin\Dta\Casting\CastIntegerValue;
use Xaamin\Dta\Casting\CastDateTimeValue;

class TemplateValueCasting
{
    protected $casts = [];

    protected $casting;

    protected $manager;

    public function __construct(CastingManager $manager = null)
    {
        $this->casting = $manager ? : new CastingManager();
    }

    public function defaults()
    {
        $integer = new CastIntegerValue();
        $float = new CastFloatValue();
        $date = new CastDateValue();
        $datetime = new CastDateTimeValue();
        $string = new CastStringValue();

        $this->casting->extend($integer);
        $this->casting->extend($float);
        $this->casting->extend($date);
        $this->casting->extend($datetime);
        $this->casting->extend($string);

        return $this;
    }

    public function make(array $data)
    {
        if (!$this->manager) {
            throw new LogicException("The casts factory is not defined.");
        }

        $this->casts = $this->manager->withData($data)->make();

        return $this->castSingle($data);
    }

    public function using(TemplateChainManager $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    protected function getData()
    {
        return $this->data;
    }

    protected function getCasts()
    {
        return $this->casts;
    }

    protected function castSingle(array $values, $prefix = null)
    {
        $result = [];

        foreach ($values as $key => $value) {
            if (is_array($value)) {
                $nextPrefix = $prefix;

                if (!is_numeric($key)) {
                    $nextPrefix = !!$prefix ? "{$prefix}.{$key}" : $key;
                }

                $result[$key] = $this->castComplex($value, $nextPrefix);
            } else {
                $castName = !!$prefix && !is_numeric($prefix) ? "{$prefix}.{$key}" : $key;

                $result[$key] = $this->getValue($castName, $value);
            }
        }

        return $result;
    }

    protected function castComplex(array $values, $prefix)
    {
        $result = [];

        foreach ($values as $key => $value) {
            if (is_array($value)) {
                $nextPrefix = !is_numeric($key) ? "{$prefix}.{$key}" : $prefix;

                $result[$key] = $this->castSingle($value, $nextPrefix);
            } else {
                $castName = !is_numeric($key) ? "{$prefix}.{$key}" : $prefix;

                $result[$key] = $this->getValue($castName, $value);
            }
        }

        return $result;
    }

    protected function getValue($castName, $value)
    {
        $cast = $this->casts[$castName] ?? null;

        if (!!$cast) {
            $params = !is_array($cast) ? explode(',', $cast) : $cast;
            $params = array_map(function ($value) {
                return trim($value);
            }, $params);

            $name = array_shift($params);

            $segments = explode(':', $name);

            if (count($segments)) {
                $name = array_shift($segments);

                $params = array_merge($segments, $params);
            }

            $value = $this->casting->get($name)->make($value, $params);
        }

        return $value;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->casting, $name], $arguments);
    }
}