<?php
namespace Xaamin\Dta\Templates;

class MultipleNodeRowMappig
{
    protected $template = [];
    protected $partial = [];

    public function __construct(array $template, array $partial)
    {
        $this->template = $template;
        $this->partial = $partial;
    }

    public function make()
    {
        $haystack = [];

        $initializer = $this->locateGroupKeyInitializer($this->template, $this->partial);

        if (!$initializer) {
            return [$haystack, $this->partial];
        }

        list($haystack, $partial) = (new GroupNodeMappig($initializer, $this->template, $this->partial))->make();

        $groups = [];

        foreach ($haystack as $group) {
            // No se reasigna partial ya que se reasignó al calcular grupos
            list($values) = (new SingleNodeRowMapping($this->template, $group))->make();

            $groups[] = $values;
        }

        return [$groups, $partial];
    }

    protected function locateGroupKeyInitializer(array $template, array $partial)
    {
        foreach ($template as $value) {
            if (is_array($value)) {
                continue;
            }

            if (in_array($value, $partial)) {
                return $value;
            }
        }

        return null;
    }

}