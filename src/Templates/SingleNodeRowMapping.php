<?php
namespace Xaamin\Dta\Templates;

class SingleNodeRowMapping
{
    protected $template = [];
    protected $partial = [];

    public function __construct(array $template, array $partial)
    {
        $this->template = $template;
        $this->partial = $partial;
    }

    public function make()
    {
        $result = [];

        foreach ($this->template as $key => $value) {
            if (is_int($key)) {
                list($index, $partial) = $this->locate($this->partial, $value);

                $result[$value] = $index;

                // Reemplazamos el template array con los valores que quedan
                $this->partial = $partial;
            } else {
                if (isset($value[0]) && is_array($value[0])) {
                    list($values, $partial) = (new MultipleNodeRowMappig($value[0], $this->partial))->make();

                    $result[$key] = $values;

                    $this->partial = $partial;
                } else {
                    list($values, $partial) = (new SingleNodeRowMapping($value, $this->partial))->make();

                    $result[$key] = $values;

                    $this->partial = $partial;
                }
            }
        }

        return [$result, $this->partial];
    }

    public function locate(array $haystack, string $needle)
    {
        $index = null;

        foreach ($haystack as $key => $value) {
            if ($value === $needle) {
                $index = $key;

                unset($haystack[$key]);
                break;
            }
        }

        return [$index, $haystack];
    }
}
