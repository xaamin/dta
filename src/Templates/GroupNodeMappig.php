<?php
namespace Xaamin\Dta\Templates;

class GroupNodeMappig
{
    protected $initializer;
    protected $template = [];
    protected $partial = [];

    public function __construct($initializer, array $template, array $partial)
    {
        $this->initializer = $initializer;
        $this->template = $template;
        $this->partial = $partial;
    }

    public function make()
    {
        $groups = [];

        $tmpGroup = [];

        $keys = $this->getKeysIncludingNestedKeysForTemplate($this->template);

        foreach ($this->partial as $key => $value) {
            if ($value === $this->initializer) {
                // Es un nuevo grupo?
                if (empty($tmpGroup)) {
                    $tmpGroup[$key] = $value;

                    unset($this->partial[$key]);
                } else {
                    // Estamos cerrando grupo para iniciar uno uevo
                    $groups[] = $tmpGroup;

                    $tmpGroup = [$key => $value];

                    unset($this->partial[$key]);
                }
            } else {
                // Buscamos valores que forman parte del grupo
                if (in_array($value, $keys)) {
                    $tmpGroup[$key] = $value;

                    unset($this->partial[$key]);
                }
            }
        }

        if (!empty($tmpGroup)) {
            $groups[] = $tmpGroup;
        }

        return [$groups, $this->partial];
    }

    protected function getKeysIncludingNestedKeysForTemplate(array $template)
    {
        $keys = [];

        foreach ($template as $value) {
            if (is_array($value)) {
                $keys = array_merge($keys, $this->getKeysIncludingNestedKeysForTemplate($value));
            } else {
                $keys[] = $value;
            }
        }

        return $keys;
    }
}