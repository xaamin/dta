<?php
namespace Xaamin\Dta;

use Xaamin\Dta\Templates\SingleNodeRowMapping;

class TemplateBuilder
{
    protected $template = [];

    public function __construct(array $template = null)
    {
        if ($template) {
            $this->setTemplate($template);
        }
    }

    public function setTemplate(array $template)
    {
        $this->template = $template;

        return $this;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function make(array $headers)
    {
        return (new SingleNodeRowMapping($this->template, $headers))->make();
    }
}
