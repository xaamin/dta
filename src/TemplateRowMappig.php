<?php
namespace Xaamin\Dta;

class TemplateRowMappig
{
    protected $template = [];

    public function __construct(array $template)
    {
        $this->template = $template;
    }

    public function make(array $row)
    {
        $result = [];

        foreach ($this->template as $key => $value) {
            if (!is_array($value)) {
                // $value es el número de columna
                $rowValue = $row[$value] ?? null;

                // QUESTION: Debe ignorarse en el input? O debería dejarse el null o ''
                if ($rowValue !== null && $rowValue !== '') {
                    $result[$key] = $rowValue;
                }
            } else {
                $values = (new TemplateRowMappig($value))->make($row);

                if (!empty($values)) {
                    if (is_numeric($key)) {
                        $result[$key] = $values;

                        $result = array_values($result);
                    } else {
                        $result[$key] = $values;
                    }
                }
            }
        }

        return $result;
    }
}
