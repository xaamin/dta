<?php
namespace Xaamin\Dta;

use Xaamin\Dta\Contracts\TemplateChainStepInterface;

class TemplateChainManager
{
    protected $data = [];
    protected $strategies = [];

    public function extend(TemplateChainStepInterface $strategy)
    {
        $this->strategies[$strategy->getName()] = $strategy;
    }

    public function getStrategies()
    {
        return $this->strategies;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function withData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public function make(array $values = [])
    {
        $result = $values;

        foreach ($this->strategies as $strategy) {
            $result = array_merge($result, $strategy->withData($this->data)->execute($result));
        }

        return $result;
    }
}
